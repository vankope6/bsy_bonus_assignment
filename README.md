# The Last Straw (aka the Bonus Assignment) Report Blog
#### *Petra Vankova, 2020/01/14*
In this blog post I'm going to describe my effort in solving the bonus assignment, the ups and downs, how luck helped me sometimes and what hint I found the most irritating :)

#### Task description
* Get the token you have received in your email
* Download the file and look into is and go to 192.168.1.167:9292
* Follow the instructions you find on the way

### Hints
First of all, I made a list of all hints we got in emails, so that I could use them later. Later in this blog, I will use only their numbers as reference.
1. _DO NOT SHARE YOUR TOKEN WITH ANYONE (for your own protection). You will use it for identification, decryption and encryption._
2. _When in doubts, try to roll back and see what have you discovered during the previous stages._
3. _Sometimes you have to use force to get things going. If that is not enough, use more force._
4. _When running scripts with nmap, be sure it doesn't stop prematurely. 'unpwdb.timelimit' might be useful for that_
5. _Choose your words carefully. If used properly, you shouldn't need more than 20000 of them._
6. _Speaking of which, take hints from the master lyrist here: https://www.youtube.com/watch?v=y3Ca3c6J9N4_

## Stage 1 - My favourite trial/error/luck method
I started with downloading the *.pcap* file, noticing it was quite long, so I went straight to the instruction server with
```bash
$ ncat 192.168.1.167 9292
```
I logged in with my secret token, and was welcomed with my first question
```bash
<Grinch> What is the IP of the C&C?
```
OK, I know what I should be looking for in the pcap file, but as curious as I am, I could not resist finding out the other questions.
They were:
```
<Grinch> How many times did the victim computer connect to the C&C IP?
<Grinch> Was any malicious executable downloaded (Yes/No)?
<Grinch> What is the DNS server IP address?
```
I tried random answers, just to go through, pressed `Ctrl+C` at the fourth one (not sure why, I learned only later from a friend, that there were only 15 attempts - I was then worried, that there were 15 attempts for all stages together, so I got pretty scared here - yes, literally). I was determined not to try anything unless I was sure. So I moved to examine the file with **Wireshark**.

First of all, I filtered by `DNS`, and the only DNS server possible was `1.1.1.1`. That would be one, the other three questions were about the C&C, so I knew I had to identify it to be able to answer. 

I first used [**Zeek/Bro**](zeek.org) to generate the `conn.log` file:
```
$ bro -r bonus_assignment_stage1.pcap
```
I then used the jupyter notebook *bro_logs_pandas.py* from [this folder](https://drive.google.com/open?id=1xvkVD2UCY4y7kSAfKVKo_st0qQfjNJaa) and I hoped to be able to find something there. 
I spent about an hour (or 2) trying to tune the script to give me something useful, but I was no smarter. The IP addresses on the left side of this graph didn't appear to be any suspicious.

![graph](images/Figure_1.png)

After sadly abandoning this approach, I went to my "favourite" naive one. I started filtering by about all the protocols I knew - `HTTP`, `TCP`, `UDP`,... and I started opening the conversations with `Follow -> xxx stream...` and I was trying to find something. As you can imagine it took a lot of time. 
I finally tried to use a filter `SSL` and saw a bit of a pattern in the times of the messages. I opened the stream of one of those lines and it looked promising!

![wireshark1](images/Screenshot3.png)
![wireshark1](images/Screenshot2.png)

Not only did the times were exactly (well, exactly rounded to) 300 seconds apart, but the message contained `DARKVNC` - I have never heard of it, but anything called *Dark* is suspicious. I googled it, and the result included **attack chain**, **mallware**, **performs activities without user's knowledge**. 
Being scared of those 15 attempts, now I was pretty confident, that I was on the right track. 
I filtered `ip.addr == 37.48.125.108`. To answer the second question, I found how many times the `[SYN, ACK]` flags were in there, which was 2, and I haven't seen anything that suggested any executable had been downloaded (yeah, I might have guessed that a bit).

After filling the answers, I got to the stage 2! First attempt! (And yeah, false alarm with the attempts:))

![hurray](images/Screenshot1.png)

## Stage 2 - How I was once again reminded that having a bachelor's in IT when starting a master's might be useful
Motivated by finishing stage 1, I went straight to stage 2.  
I started with the number `3232235903`. Honestly, it could be anything, a password, a file size, number of seconds... So I just asked Google. And it appeared to be a good choice. I got an IP address `192.168.1.127`.

I then tried to 'solve' the Hint as well. I saw the pattern in it, so I looked for a simple cipher, I tried to remove the *Ms*, then any other character, that was repeated, but I only got 'cgkE', which means obviously nothing (okay, don't laugh). 

Google didn't help, so I decided to have a break. While doing something work related, I copied my ssh public key to a server and saw that `=` at the end! After finding out that it's base64, which I should have known, but somehow I did not (and I take the blame), I used [this page](cryptii.com) to convert it to hexadecimal bytes, and the same page to convert those bytes to integers and I got:

![convert](images/Screenshot4.png)

Well that looked like port numbers, I tried to connect with `ncat` to them, but it didn't work. I tried `nmap` with range of ports, which didn't work either, but it gave me a tip to use `-Pn`:

![Pn](images/Screenshot5.png)

It returned with 4 open port - **22**, **902**, **6667** and **8081**. I could not connect to **22** and **902**, so I tried **6667**. I am not a big fan of SW, so I stayed for a couple of scenes only:) And **8081** didn't seem to help either. 

I thought of using the force from hint 3, but I didn't have any clue about a username, so there was no point in brute-forcing. 
After some time googling and trying to figure out what to do next, I reread all the hints multiple times, I went back to the end of stage one `Knock, knock ... Your VM might be handy.` Well, I already used the VM for the first stage, that was weird. I finally googled `knock network` expecting to find nothing... PORT KNOCKING? I had never heard of that before.
After learning what that is, I found a [script](https://github.com/grongor/knock) in Python,
but that didn't seem to work. Then I used a bash script found [here](https://wiki.archlinux.org/index.php/Port_knocking) 
```
#!/bin/bash
HOST=$1
shift
for ARG in "$@"
do
        nmap -Pn --host-timeout 100 --max-retries 0 -p $ARG $HOST
done
```
but nothing happened, I found another one [here](https://stackoverflow.com/questions/32810683/bash-port-knock-with-combination), which is just the first one with a for cycle, but nothing happened either. 
![Portknock](images/Screenshot13.png)

Yes, I expected a notification:)
After some time I finally tried to scan for open ports again, and one more appeared - **8080**. With `curl`, I got the CVE hint:
![cve](images/Screenshot14.png)
It was pretty straight-forward after opening the link. At first I tried:
```
$ curl -H "user-agent: () { :; }; echo; echo; /bin/bash -c 'ls'" http://your-ip:8080/cgi-bin/stats
$ curl -H "user-agent: () { :; }; echo; echo; /bin/bash -c 'ls /'" http://your-ip:8080/cgi-bin/stats
```
and then:

![present](images/Screenshot16.png)

In the file with my CVUT username, there was this:
```
[a-zA-Z0-9]{3} symetric:
8befb49435c9e07fb67f08f7671ab3d7a4cfa0a7cdb837cd71bf31ab33697b9b7fc61982adf0cae9bd06fdcf2c5f837fa72085a24be9de73788accef248d947eea1f08087c3ef4c722c5c59ff03ef1efb668d0dd21e208c86192cf09eb6a87360e0481765c0cff9076c60bbfb33c7e29b488ee0ee868fe4155d25721568065026a16143a0cf16e15b5c3704fcf294abc25ca45a288b2d01720ed0672712886b4f4b380
```

Comming from a cryptographic background, I saw it right away - cipher! I expected it to be something well known, so symmetric - AES, DES, RC4? Number of characters didn't match any possible AES block size, since DES is a block cipher as well, I went straight to RC4 stream cipher. Using [this page](https://www.dcode.fr/rc4-cipher) I got the text I needed:
![rc4](images/Screenshot18.png)

## Stage 3 - My laptop survived
Who would not want to find out what is so special about Buddy!
Since I got no other hint apart from *his home folder*, I though right away about the *ports 22/902*. From **hint 3**, sometimes we need to use force to get things going - well, we could use the brute-force here finally! Also, from **hint 4** we need to use *nmap* with `unpwdb.timelimit` arg at one moment, which could be now and also in **hint 5** - no more than *20000* words - passwords probably.
At first, I just googled **the most used passwords**, downloaded [this file](https://github.com/danielmiessler/SecLists/blob/master/Passwords/Common-Credentials/10-million-password-list-top-100000.txt) and ran 
```
$ sudo nmap -sS -sV -p 22 192.168.1.127 -v -n --script ssh-brute --script-args userdb=user_bonus,passdb=passwds_20k
```
where `user_bonus` contained `Buddy`, and `passwds_20k` contained top 20k from the downloaded file. But it gave me no valid credentials. 

I thought I might have a wrong password list. After some googling I went to hints again and there it was - **the lyrics** (hint 6). So I googled `we will rock you passwords` and downloaded `rockyou.txt` file from [here](https://wiki.skullsecurity.org/Passwords). I also added `buddy` to the `user_bonus` list to be sure. 
I ran the brute-force script again, but it returned no valid accounts again. Then I spotted that it ran for only about 900 seconds and tried about 900-1000 passwords.
I then remembered the hint 4 - the `unpwdb.timelimit` arg. But now it stopped right away. I ran it a few times again, but I got the same results.

I spent 2-3 frustrating hours trying to get it going. Meanwhile I noticed, that it always stopped saying that port 22 was **filtered**, but when I ran simple `nmap` to find open ports, 22 was always there as **open**.  (This is the only thing I forgot to take a screenshot of, but I swear it's true.) I also tried to brute force 902, but it stopped right away, saying that 902 was **closed**. Google did not help me either.
This was my mood then:

![doomed](images/Screenshot8.png)

When I was almost about to give up (or throw my laptop out of a window), it suddenly worked ¯\_(ツ)_/¯. Well, if this was the hint 3 - **if that is not enough, use more force**, it was not funny, not at all:) Anyway, this was the whole command:
```
$ sudo nmap -sS -sV -p 22 192.168.1.127 -v -n --script ssh-brute --script-args userdb=user_bonus,passdb=pass_20k,unpwdb.timelimit=0
```
Thanks to the timelimit argument I could let it run overnight. 
And in the morning I got the results!

![valid](images/Screenshot21.png)

So I logged in the server with
``` 
$ ssh buddy@192.168.1.127
```
using the password and that was it - end of stage 3!

![stage3](images/Screenshot22.png)

## Stage 4 - Maybe next time
I looked into the `Stocking` file at the instructions.

I knew there were 4 stages at this moment, and because of a lack of time and my feelings from the first three parts, I decided not to try doing stage 4. Even though I might very well look at it later just to try it out. 

## Summary
This was without a doubt the most challenging task of all. The first stage was something I kind of already knew from a previous task, but I am still not that satisfied with how I got the results. The second stage was especially hard for me, I think it was supposed to be quite easy, but I still have a lot to learn. I would consider the third part most exciting for me (when I forget those hours I could not get the brute force work). 
But even though I really struggled with each part, it was fun!

